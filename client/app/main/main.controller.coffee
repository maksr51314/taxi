'use strict'

angular.module 'taxiCallApp'
.controller 'MainCtrl', ($scope, $http, socket) ->

  style = [
    {
      stylers: [
        { saturation: "-100" },
        { lightness: "20" }
      ]
    },{
      featureType: "poi",
      stylers: [
        { visibility: "off" }
      ]
    },{
      featureType: "transit",
      stylers: [
        { visibility: "off" }
      ]
    },{
      featureType: "road",
      stylers: [
        { lightness: "50" },
        { visibility: "on" }
      ]
    },{
      featureType: "landscape",
      stylers: [
        { lightness: "50" }
      ]
    }
  ]

  markers = []

  map = new GMaps(
    el: '#map',
    lat: 50.4267,
    lng: 30.5326905 + 30.53*0.0008,
    styles: style,
    disableDefaultUI: true
#  delete map button from map
    mapTypeControlOptions: {
      mapTypeIds: []
    },

#  default zoom
#    zoom: 11
    zoom: 13
  );

  inputPlaceFrom = document.getElementById('inputPlaceFrom');
  inputPlaceTo = document.getElementById('inputPlaceTo');

  map.controls.push(inputPlaceFrom)
  map.controls.push(inputPlaceTo)

  inputPlaceFromSearchBox = new google.maps.places.SearchBox(inputPlaceFrom)
  inputPlaceToSearchBox = new google.maps.places.SearchBox(inputPlaceTo)

  google.maps.event.addListener(inputPlaceFromSearchBox, 'places_changed', ()=>
    places = inputPlaceFromSearchBox.getPlaces()

    if places.length
      undefined

#    marker.setMap(null) for marker in markers

    bounds = new google.maps.LatLngBounds();

    setMarker = (place) ->
#      marker = new google.maps.Marker(
#        map : map
#        icon: image
#        title: place.name
#        position: place.geometry.location
#      )
#      markers.push(marker)
      bounds.extend(place.geometry.location)

    setMarker(place) for place in places

    map.fitBounds(bounds);
  )

#  Simple road
  map.drawRoute(
    origin:      [50.4267, 30.5326905],
    destination: [50.4267, 30.53],
    travelMode:  'driving',
    strokeColor: '#131540',
    strokeOpacity: 0.6,
    strokeWeight: 6
  );

#  class MainCtrl
#    constructor: () ->
#      $scope.Template =
#        MAP : 'mapRoute'

#  return new MainCtrl()

#  $scope.awesomeThings = []
#
#  $http.get('/api/things').success (awesomeThings) ->
#    $scope.awesomeThings = awesomeThings
#    socket.syncUpdates 'thing', $scope.awesomeThings
#
#  $scope.addThing = ->
#    return if $scope.newThing is ''
#    $http.post '/api/things',
#      name: $scope.newThing
#
#    $scope.newThing = ''
#
#  $scope.deleteThing = (thing) ->
#    $http.delete '/api/things/' + thing._id
#
#  $scope.$on '$destroy', ->
#    socket.unsyncUpdates 'thing'
